# translation of kwin4.po to Telugu
# విజయ్ కిరణ్ కముజు <infyquest@yahoo.com>, 2007.
# V Naresh Kumar <cyan_00391@yahoo.co.in>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: kwin4\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-28 00:43+0000\n"
"PO-Revision-Date: 2007-07-19 15:17+0530\n"
"Last-Translator: V Naresh Kumar <cyan_00391@yahoo.co.in>\n"
"Language-Team: Telugu <en@li.org>\n"
"Language: te\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""

#, kde-format
msgctxt "Player 0 color"
msgid "Black"
msgstr ""

#, fuzzy, kde-format
#| msgid " - Yellow "
msgctxt "Player 0 color"
msgid "Yellow"
msgstr " - పసుపు "

#, fuzzy, kde-format
#| msgid "Red"
msgctxt "Player 1 color"
msgid "Red"
msgstr "ఎరుపు"

#: chatdlg.cpp:33
#, fuzzy, kde-format
#| msgid "Chat"
msgctxt "@title:window"
msgid "Chat Dlg"
msgstr "చాట్"

#: chatdlg.cpp:48
#, kde-format
msgid "Chat"
msgstr "చాట్"

#: chatdlg.cpp:54
#, kde-format
msgid "Configure..."
msgstr "అమర్చు..."

#: displayintro.cpp:72
#, kde-format
msgctxt "Name of quicklaunch field"
msgid "Quick Launch"
msgstr ""

#: displayintro.cpp:79
#, kde-format
msgctxt "Ask player who should start game"
msgid "Who starts?"
msgstr ""

#: displayintro.cpp:83
#, kde-format
msgctxt "Ask player which color he wants to play"
msgid "Your color?"
msgstr ""

#: displayintro.cpp:127
#, fuzzy, kde-format
#| msgid "Easy"
msgctxt "quick start button - player versus AI level easy"
msgid "Easy Game"
msgstr "తేలిక"

#: displayintro.cpp:133
#, fuzzy, kde-format
#| msgid "No game  "
msgctxt "quick start button - player versus AI level normal"
msgid "Normal Game"
msgstr "ఆట లేదు"

#: displayintro.cpp:139
#, fuzzy, kde-format
#| msgid "Hard"
msgctxt "quick start button - player versus AI level hard"
msgid "Hard Game"
msgstr "కష్టము"

#: displayintro.cpp:145
#, fuzzy, kde-format
#| msgid "KWin4: Two player network game"
msgctxt "quick start button - player versus player"
msgid "Two Player Game"
msgstr "కెవిన్4 : ఇద్దరు ఆటగాళ్ల నెట్వర్క్ ఆట"

#: kchatdialog.cpp:80
#, fuzzy, kde-format
#| msgid "Configure..."
msgctxt "@title:window"
msgid "Configure Chat"
msgstr "అమర్చు..."

#: kchatdialog.cpp:92 kchatdialog.cpp:114
#, kde-format
msgid "Name Font..."
msgstr ""

#: kchatdialog.cpp:95 kchatdialog.cpp:117
#, kde-format
msgid "Text Font..."
msgstr ""

#: kchatdialog.cpp:104
#, fuzzy, kde-format
#| msgid "Player 1"
msgid "Player: "
msgstr "మొదటి ఆటగాడు"

#: kchatdialog.cpp:106
#, kde-format
msgid "This is a player message"
msgstr ""

#: kchatdialog.cpp:112
#, kde-format
msgid "System Messages - Messages directly sent from the game"
msgstr ""

#: kchatdialog.cpp:126
#, kde-format
msgid "--- Game: "
msgstr ""

#: kchatdialog.cpp:128
#, kde-format
msgid "This is a system message"
msgstr ""

#: kchatdialog.cpp:132
#, kde-format
msgid "Maximum number of messages (-1 = unlimited):"
msgstr ""

#. i18n: ectx: Menu (game)
#: kfourinlineui.rc:10
#, fuzzy, kde-format
#| msgid "Name"
msgid "&Game"
msgstr "పేరు"

#. i18n: ectx: Menu (settings)
#: kfourinlineui.rc:18
#, kde-format
msgid "&Settings"
msgstr ""

#. i18n: ectx: ToolBar (mainToolBar)
#: kfourinlineui.rc:24
#, kde-format
msgid "Main Toolbar"
msgstr ""

#: kgameconnectdialog.cpp:63
#, fuzzy, kde-format
#| msgid "The network game ended!\n"
msgid "Create a network game"
msgstr "నెట్వర్క్ ఆట ఆగిపోయింది!\n"

#: kgameconnectdialog.cpp:66
#, fuzzy, kde-format
#| msgid "The network game ended!\n"
msgid "Join a network game"
msgstr "నెట్వర్క్ ఆట ఆగిపోయింది!\n"

#: kgameconnectdialog.cpp:72
#, fuzzy, kde-format
#| msgid "Game name"
msgid "Game name:"
msgstr "ఆట పేరు"

#: kgameconnectdialog.cpp:74
#, fuzzy, kde-format
#| msgid "Network Chat..."
msgid "Network games:"
msgstr "నెట్వర్క్ ద్వారా చాట్ చెయి..."

#: kgameconnectdialog.cpp:84
#, kde-format
msgid "Port to connect to:"
msgstr ""

#: kgameconnectdialog.cpp:90
#, kde-format
msgid "Host to connect to:"
msgstr ""

#: kgameconnectdialog.cpp:95
#, kde-format
msgid "&Start Network"
msgstr ""

#: kgamedebugdialog.cpp:118
#, kde-format
msgctxt "@title:window"
msgid "KGame Debug Dialog"
msgstr ""

#: kgamedebugdialog.cpp:140
#, fuzzy, kde-format
#| msgid "Debug KGame"
msgid "Debug &KGame"
msgstr "కెగేమ్ ను డీబగ్ చెయ్యండి"

#: kgamedebugdialog.cpp:162 kgamedebugdialog.cpp:215
#, kde-format
msgid "Update"
msgstr ""

#: kgamedebugdialog.cpp:167
#, fuzzy, kde-format
#| msgid "Game name"
msgid "KGame Pointer"
msgstr "ఆట పేరు"

#: kgamedebugdialog.cpp:168
#, fuzzy, kde-format
#| msgid "Name"
msgid "Game ID"
msgstr "పేరు"

#: kgamedebugdialog.cpp:169
#, fuzzy, kde-format
#| msgid "Game name"
msgid "Game Cookie"
msgstr "ఆట పేరు"

#: kgamedebugdialog.cpp:170
#, kde-format
msgid "Is Master"
msgstr ""

#: kgamedebugdialog.cpp:171
#, kde-format
msgid "Is Admin"
msgstr ""

#: kgamedebugdialog.cpp:172
#, kde-format
msgid "Is Offering Connections"
msgstr ""

#: kgamedebugdialog.cpp:173
#, fuzzy, kde-format
#| msgid "Game name"
msgid "Game Status"
msgstr "ఆట పేరు"

#: kgamedebugdialog.cpp:174
#, fuzzy, kde-format
#| msgid "Game running..."
msgid "Game is Running"
msgstr "ఆట కొనసాగుతుంది..."

#: kgamedebugdialog.cpp:175
#, kde-format
msgid "Maximal Players"
msgstr ""

#: kgamedebugdialog.cpp:176
#, kde-format
msgid "Minimal Players"
msgstr ""

#: kgamedebugdialog.cpp:177
#, fuzzy, kde-format
#| msgid "Player 1"
msgid "Players"
msgstr "మొదటి ఆటగాడు"

#: kgamedebugdialog.cpp:183
#, fuzzy, kde-format
#| msgid "Debug KGame"
msgid "Debug &Players"
msgstr "కెగేమ్ ను డీబగ్ చెయ్యండి"

#: kgamedebugdialog.cpp:193
#, kde-format
msgid "Available Players"
msgstr ""

#: kgamedebugdialog.cpp:219
#, fuzzy, kde-format
#| msgid "Player 1 name"
msgid "Player Pointer"
msgstr "మొదటి ఆటగాడి పేరు"

#: kgamedebugdialog.cpp:220
#, fuzzy, kde-format
#| msgid "Player 1"
msgid "Player ID"
msgstr "మొదటి ఆటగాడు"

#: kgamedebugdialog.cpp:221
#, fuzzy, kde-format
#| msgid "Player Names"
msgid "Player Name"
msgstr "ఆటగాళ్ల పేర్లు"

#: kgamedebugdialog.cpp:222
#, fuzzy, kde-format
#| msgid "Player 1"
msgid "Player Group"
msgstr "మొదటి ఆటగాడు"

#: kgamedebugdialog.cpp:223
#, fuzzy, kde-format
#| msgid "Player 1"
msgid "Player User ID"
msgstr "మొదటి ఆటగాడు"

#: kgamedebugdialog.cpp:224
#, kde-format
msgid "My Turn"
msgstr ""

#: kgamedebugdialog.cpp:225
#, kde-format
msgid "Async Input"
msgstr ""

#: kgamedebugdialog.cpp:226
#, kde-format
msgid "KGame Address"
msgstr ""

#: kgamedebugdialog.cpp:227
#, fuzzy, kde-format
#| msgid "Player 1 name"
msgid "Player is Virtual"
msgstr "మొదటి ఆటగాడి పేరు"

#: kgamedebugdialog.cpp:228
#, fuzzy, kde-format
#| msgid "Player 1 name"
msgid "Player is Active"
msgstr "మొదటి ఆటగాడి పేరు"

#: kgamedebugdialog.cpp:229
#, kde-format
msgid "RTTI"
msgstr ""

#: kgamedebugdialog.cpp:230
#, fuzzy, kde-format
#| msgid "Network connection port"
msgid "Network Priority"
msgstr "నెట్వర్క్ పోర్ట్"

#: kgamedebugdialog.cpp:236
#, fuzzy, kde-format
#| msgid "Debug KGame"
msgid "Debug &Messages"
msgstr "కెగేమ్ ను డీబగ్ చెయ్యండి"

#: kgamedebugdialog.cpp:251
#, kde-format
msgid "&>>"
msgstr ""

#: kgamedebugdialog.cpp:255
#, kde-format
msgid "&<<"
msgstr ""

#: kgamedebugdialog.cpp:259
#, kde-format
msgid "Do not show IDs:"
msgstr ""

#: kgamedebugdialog.cpp:328
#, kde-format
msgid "NULL pointer"
msgstr ""

#: kgamedebugdialog.cpp:338 kgamedebugdialog.cpp:339 kgamedebugdialog.cpp:340
#: kgamedebugdialog.cpp:342 kgamedebugdialog.cpp:395 kgamedebugdialog.cpp:396
#: kgamedebugdialog.cpp:399 kgamedebugdialog.cpp:400
#, kde-format
msgid "True"
msgstr ""

#: kgamedebugdialog.cpp:338 kgamedebugdialog.cpp:339 kgamedebugdialog.cpp:340
#: kgamedebugdialog.cpp:342 kgamedebugdialog.cpp:395 kgamedebugdialog.cpp:396
#: kgamedebugdialog.cpp:399 kgamedebugdialog.cpp:400
#, kde-format
msgid "False"
msgstr ""

#: kgamedebugdialog.cpp:354 kgamedebugdialog.cpp:412
#, kde-format
msgid "Clean"
msgstr ""

#: kgamedebugdialog.cpp:357 kgamedebugdialog.cpp:415
#, kde-format
msgid "Dirty"
msgstr ""

#: kgamedebugdialog.cpp:360 kgamedebugdialog.cpp:418
#, kde-format
msgid "Local"
msgstr ""

#: kgamedebugdialog.cpp:364 kgamedebugdialog.cpp:422
#, kde-format
msgid "Undefined"
msgstr ""

#: kgamedebugdialog.cpp:507
#, kde-format
msgid "Unknown"
msgstr ""

#: kgamedialog.cpp:92
#, fuzzy, kde-format
#| msgid "Network Chat..."
msgid "&Network"
msgstr "నెట్వర్క్ ద్వారా చాట్ చెయి..."

#: kgamedialogconfig.cpp:124
#, kde-format
msgid "Disconnect"
msgstr ""

#: kgamedialogconfig.cpp:128 kwin4.cpp:680
#, kde-format
msgid "Network Configuration"
msgstr "నెట్వర్క్ అమరిక"

#: kgamedialogconfig.cpp:187
#, kde-format
msgid "Cannot connect to the network"
msgstr ""

#: kgamedialogconfig.cpp:193
#, kde-format
msgid "Network status: No Network"
msgstr ""

#: kgamedialogconfig.cpp:199
#, kde-format
msgid "Network status: You are MASTER"
msgstr ""

#: kgamedialogconfig.cpp:201
#, kde-format
msgid "Network status: You are connected"
msgstr ""

#: kwin4.cpp:74
#, kde-format
msgid "Installation error: No theme list found."
msgstr ""

#: kwin4.cpp:101
#, kde-format
msgid "Installation error: No AI engine found. Continue without AI."
msgstr ""

#: kwin4.cpp:116
#, kde-format
msgid "Installation error: Theme file error."
msgstr ""

#: kwin4.cpp:264
#, fuzzy, kde-format
#| msgid "Aborts a currently played game. No winner will be declared."
msgid "Ends a currently played game. No winner will be declared."
msgstr "ప్రస్తుత ఆట ఆపివేయబడును. ఎవరూ గెలవలేదు"

#: kwin4.cpp:269
#, kde-format
msgid "&Network Configuration..."
msgstr "(&N) నెట్వర్క్ అమరిక..."

#: kwin4.cpp:273
#, kde-format
msgid "Network Chat..."
msgstr "నెట్వర్క్ ద్వారా చాట్ చెయి..."

#: kwin4.cpp:278
#, kde-format
msgid "&Show Statistics"
msgstr "(&S) గణాంకాలను చూపు"

#: kwin4.cpp:280
#, kde-format
msgid "Show statistics."
msgstr "గణాంకాలను చూపు"

#: kwin4.cpp:292
#, kde-format
msgid "Theme"
msgstr ""

#: kwin4.cpp:303
#, kde-format
msgid "Debug KGame"
msgstr "కెగేమ్ ను డీబగ్ చెయ్యండి"

#: kwin4.cpp:326
#, fuzzy, kde-format
#| msgid "Welcome to KWin4"
msgid "Welcome to Four Wins"
msgstr "కెవిన్4 కు స్వాగతం"

#: kwin4.cpp:486 kwin4.cpp:639
#, kde-format
msgid "Game running..."
msgstr "ఆట కొనసాగుతుంది..."

#: kwin4.cpp:600
#, kde-format
msgid "No game  "
msgstr "ఆట లేదు"

#: kwin4.cpp:602 kwin4.cpp:604
#, fuzzy, kde-format
#| msgid " - Red "
msgid " %1 - %2 "
msgstr " - ఎరుపు "

#: kwin4.cpp:606
#, kde-format
msgid "Nobody  "
msgstr "ఎవరులేరు "

#: kwin4.cpp:628
#, kde-format
msgid "The network game ended!\n"
msgstr "నెట్వర్క్ ఆట ఆగిపోయింది!\n"

#: kwin4.cpp:649
#, kde-format
msgid "The game is drawn. Please restart next round."
msgstr "ఉభయత్ర గెలుపులేని ఆట."

#: kwin4.cpp:656
#, kde-format
msgid "%1 won the game. Please restart next round."
msgstr ""

#: kwin4.cpp:661
#, fuzzy, kde-format
#| msgid "The game is drawn. Please restart next round."
msgid " Game ended. Please restart next round."
msgstr "ఉభయత్ర గెలుపులేని ఆట."

#: kwin4.cpp:691
#, fuzzy, kde-format
#| msgid "Red should be played by remote"
msgid "Black should be played by remote player"
msgstr "ఎరుపు రంగు అవతలి ఆటగాడు వాడాలి"

#: kwin4.cpp:692
#, fuzzy, kde-format
#| msgid "Red should be played by remote"
msgid "Red should be played by remote player"
msgstr "ఎరుపు రంగు అవతలి ఆటగాడు వాడాలి"

#: kwin4.cpp:769 kwin4.cpp:770 kwin4.cpp:791 kwin4.cpp:792
#, fuzzy, kde-format
#| msgid "Red Plays With"
msgid "%1 Plays With"
msgstr "ఎరుపు ఆటగాడు వాడేది"

#: kwin4.cpp:793
#, kde-format
msgid "General"
msgstr "సామాన్య"

#. i18n: ectx: label, entry (port), group (Parameter)
#: kwin4.kcfg:10
#, kde-format
msgid "Network connection port"
msgstr "నెట్వర్క్ పోర్ట్"

#. i18n: ectx: label, entry (gamename), group (Parameter)
#: kwin4.kcfg:14
#, kde-format
msgid "Game name"
msgstr "ఆట పేరు"

#. i18n: ectx: label, entry (host), group (Parameter)
#: kwin4.kcfg:17
#, kde-format
msgid "Network connection host"
msgstr "నెట్వర్క్ కంప్యూటర్ పేరు"

#. i18n: ectx: property (whatsThis), widget (QSlider, kcfg_level)
#. i18n: ectx: label, entry (level), group (Parameter)
#: kwin4.kcfg:21 settings.ui:50
#, kde-format
msgid "Change the strength of the computer player."
msgstr "కంప్యూటర్ ఆటగాడి బలాన్ని మార్చు"

#. i18n: ectx: label, entry (Name1), group (Parameter)
#: kwin4.kcfg:28
#, kde-format
msgid "Player 1 name"
msgstr "మొదటి ఆటగాడి పేరు"

#: kwin4.kcfg:32
#, fuzzy, kde-format
#| msgid "Player 1"
msgctxt "default name of first player"
msgid "Player 1"
msgstr "మొదటి ఆటగాడు"

#. i18n: ectx: label, entry (Name2), group (Parameter)
#: kwin4.kcfg:35
#, kde-format
msgid "Player 2 name"
msgstr "రెండవ ఆటగాడి పేరు"

#: kwin4.kcfg:39
#, fuzzy, kde-format
#| msgid "Player 2"
msgctxt "default name of second player"
msgid "Player 2"
msgstr "రెండవ ఆటగాడు"

#: main.cpp:67
#, kde-format
msgid "KFourInLine"
msgstr ""

#: main.cpp:69
#, fuzzy, kde-format
#| msgid "KWin4: Two player network game"
msgid "KFourInLine: Two player board game"
msgstr "కెవిన్4 : ఇద్దరు ఆటగాళ్ల నెట్వర్క్ ఆట"

#: main.cpp:71
#, fuzzy, kde-format
#| msgid "(c) Martin Heni   "
msgid "(c) 1995-2007, Martin Heni"
msgstr "(c) మార్టిన్ హెని   "

#: main.cpp:72
#, fuzzy, kde-format
#| msgid "(c) Martin Heni   "
msgid "Martin Heni"
msgstr "(c) మార్టిన్ హెని   "

#: main.cpp:72
#, kde-format
msgid "Game design and code"
msgstr ""

#: main.cpp:73
#, kde-format
msgid "Johann Ollivier Lapeyre"
msgstr ""

#: main.cpp:73 main.cpp:74
#, kde-format
msgid "Graphics"
msgstr ""

#: main.cpp:74
#, kde-format
msgid "Eugene Trounev"
msgstr ""

#: main.cpp:75
#, kde-format
msgid "Benjamin Meyer"
msgstr ""

#: main.cpp:75
#, kde-format
msgid "Code Improvements"
msgstr "కోడ్ అభివృద్ది చేసినది"

#: main.cpp:80
#, kde-format
msgid "Enter debug level"
msgstr "డీబగ్ చేయవలసిన స్థాయి"

#: main.cpp:81
#, kde-format
msgid "Skip intro animation"
msgstr ""

#: main.cpp:82
#, kde-format
msgid "Run game in demo (autoplay) mode"
msgstr ""

#: scoresprite.cpp:163
#, fuzzy, kde-format
#| msgid "Level"
msgctxt "computer level"
msgid "Level %1"
msgstr "స్ధాయి"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox1)
#: settings.ui:38
#, kde-format
msgid "Computer Difficulty"
msgstr "కంప్యూటర్ ప్రతిబంధకం"

#. i18n: ectx: property (text), widget (QLabel, textLabel1)
#: settings.ui:83
#, kde-format
msgid "Easy"
msgstr "తేలిక"

#. i18n: ectx: property (text), widget (QLabel, textLabel2)
#: settings.ui:90
#, kde-format
msgid "Hard"
msgstr "కష్టము"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_learning)
#: settings.ui:102
#, kde-format
msgid "Use AI learning"
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, Input1)
#: settings.ui:112
#, kde-format
msgid "Red Plays With"
msgstr "ఎరుపు ఆటగాడు వాడేది"

#. i18n: ectx: property (text), widget (QRadioButton, kcfg_input1mouse)
#. i18n: ectx: property (text), widget (QRadioButton, kcfg_input0mouse)
#: settings.ui:132 settings.ui:181
#, kde-format
msgid "Mouse"
msgstr "మౌస్"

#. i18n: ectx: property (text), widget (QRadioButton, kcfg_input1key)
#. i18n: ectx: property (text), widget (QRadioButton, kcfg_input0key)
#: settings.ui:142 settings.ui:191
#, kde-format
msgid "Keyboard"
msgstr "కీబోర్డ్"

#. i18n: ectx: property (text), widget (QRadioButton, kcfg_input1ai)
#. i18n: ectx: property (text), widget (QRadioButton, kcfg_input0ai)
#: settings.ui:149 settings.ui:198
#, kde-format
msgid "Computer"
msgstr "కంప్యూటర్"

#. i18n: ectx: property (title), widget (QGroupBox, Input0)
#: settings.ui:161
#, fuzzy, kde-format
#| msgid "Red Plays With"
msgid "Black Plays With"
msgstr "ఎరుపు ఆటగాడు వాడేది"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox2)
#: settings.ui:216
#, kde-format
msgid "Player Names"
msgstr "ఆటగాళ్ల పేర్లు"

#. i18n: ectx: property (text), widget (QLabel, textLabel3)
#: settings.ui:242
#, kde-format
msgid "Player 1:"
msgstr "మొదటి ఆటగాడు:"

#. i18n: ectx: property (text), widget (QLabel, textLabel4)
#: settings.ui:249
#, kde-format
msgid "Player 2:"
msgstr "రెండవ ఆటగాడు:"

#. i18n: ectx: property (title), widget (QGroupBox, StartColour)
#: settings.ui:267
#, kde-format
msgid "Starting Player Color"
msgstr "మొదటి ఆటగాడు ఎంచుకున్న రంగు"

#. i18n: ectx: property (text), widget (QRadioButton, kcfg_startcolourred)
#: settings.ui:287
#, kde-format
msgid "Red"
msgstr "ఎరుపు"

#. i18n: ectx: property (text), widget (QRadioButton, kcfg_startcolouryellow)
#: settings.ui:297
#, kde-format
msgid "Black"
msgstr ""

#. i18n: ectx: property (windowTitle), widget (QDialog, Statistics)
#: statistics.ui:13
#, kde-format
msgid "Statistics"
msgstr "గణాంకాలు"

#. i18n: ectx: property (text), widget (QLabel, p1_name)
#: statistics.ui:57 statuswidget.ui:113
#, kde-format
msgid "Player 1"
msgstr "మొదటి ఆటగాడు"

#. i18n: ectx: property (text), widget (QLabel, Name)
#: statistics.ui:64
#, kde-format
msgid "Name"
msgstr "పేరు"

#. i18n: ectx: property (text), widget (QLabel, won)
#: statistics.ui:74
#, kde-format
msgid "Won"
msgstr "గెలిచినవి"

#. i18n: ectx: property (text), widget (QLabel, lost)
#: statistics.ui:87
#, kde-format
msgid "Lost"
msgstr "ఓడిపోయినవి"

#. i18n: ectx: property (text), widget (QLabel, sum)
#: statistics.ui:103
#, kde-format
msgid "Sum"
msgstr "మొత్తము"

#. i18n: ectx: property (text), widget (QLabel, aborted)
#: statistics.ui:113
#, kde-format
msgid "Aborted"
msgstr "ఆపివేయబడినది"

#. i18n: ectx: property (text), widget (QPushButton, pushButton1)
#: statistics.ui:131
#, kde-format
msgid "Clear All Statistics"
msgstr "గణాంకాలను చెరిపివేయండి"

#. i18n: ectx: property (text), widget (QPushButton, pushButton2)
#: statistics.ui:154
#, kde-format
msgid "&Close"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, p2_name)
#: statistics.ui:166 statuswidget.ui:220
#, kde-format
msgid "Player 2"
msgstr "రెండవ ఆటగాడు"

#. i18n: ectx: property (text), widget (QLabel, drawn)
#: statistics.ui:176
#, kde-format
msgid "Drawn"
msgstr "ఉభయత్ర గెలుపులేని ఆట"

#. i18n: ectx: property (text), widget (QLabel, wins)
#: statuswidget.ui:63
#, kde-format
msgid "W"
msgstr "W"

#. i18n: ectx: property (text), widget (QLabel, draws)
#: statuswidget.ui:73
#, kde-format
msgid "D"
msgstr "D"

#. i18n: ectx: property (text), widget (QLabel, loses)
#: statuswidget.ui:83
#, kde-format
msgid "L"
msgstr "L"

#. i18n: ectx: property (text), widget (QLabel, num)
#: statuswidget.ui:93
#, kde-format
msgid "No"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, bk)
#: statuswidget.ui:103
#, kde-format
msgid "Bk"
msgstr "Bk"

#~ msgid "Ready"
#~ msgstr "సిద్ధము"
