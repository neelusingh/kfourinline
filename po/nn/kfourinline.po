# Translation of kfourinline to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2008, 2009, 2010, 2016, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kfourinline\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-28 00:43+0000\n"
"PO-Revision-Date: 2022-06-19 13:58+0200\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.04.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Karl Ove Hufthammer"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "karl@huftis.org"

#, kde-format
msgctxt "Player 0 color"
msgid "Black"
msgstr "Svart"

#, kde-format
msgctxt "Player 0 color"
msgid "Yellow"
msgstr "Gul"

#, kde-format
msgctxt "Player 1 color"
msgid "Red"
msgstr "Raud"

#: chatdlg.cpp:33
#, kde-format
msgctxt "@title:window"
msgid "Chat Dlg"
msgstr "Pratedialog"

#: chatdlg.cpp:48
#, kde-format
msgid "Chat"
msgstr "Prat"

#: chatdlg.cpp:54
#, kde-format
msgid "Configure..."
msgstr "Set opp …"

#: displayintro.cpp:72
#, kde-format
msgctxt "Name of quicklaunch field"
msgid "Quick Launch"
msgstr "Snøggstart"

#: displayintro.cpp:79
#, kde-format
msgctxt "Ask player who should start game"
msgid "Who starts?"
msgstr "Kven startar?"

#: displayintro.cpp:83
#, kde-format
msgctxt "Ask player which color he wants to play"
msgid "Your color?"
msgstr "Fargen din?"

#: displayintro.cpp:127
#, kde-format
msgctxt "quick start button - player versus AI level easy"
msgid "Easy Game"
msgstr "Lett"

#: displayintro.cpp:133
#, kde-format
msgctxt "quick start button - player versus AI level normal"
msgid "Normal Game"
msgstr "Middels"

#: displayintro.cpp:139
#, kde-format
msgctxt "quick start button - player versus AI level hard"
msgid "Hard Game"
msgstr "Vanskeleg"

#: displayintro.cpp:145
#, kde-format
msgctxt "quick start button - player versus player"
msgid "Two Player Game"
msgstr "To spelarar"

#: kchatdialog.cpp:80
#, kde-format
msgctxt "@title:window"
msgid "Configure Chat"
msgstr "Set opp prat"

#: kchatdialog.cpp:92 kchatdialog.cpp:114
#, kde-format
msgid "Name Font..."
msgstr "Skrifttype for namn …"

#: kchatdialog.cpp:95 kchatdialog.cpp:117
#, kde-format
msgid "Text Font..."
msgstr "Skrifttype for tekst …"

#: kchatdialog.cpp:104
#, kde-format
msgid "Player: "
msgstr "Spelar: "

#: kchatdialog.cpp:106
#, kde-format
msgid "This is a player message"
msgstr "Dette er ei spelarmelding"

#: kchatdialog.cpp:112
#, kde-format
msgid "System Messages - Messages directly sent from the game"
msgstr "Systemmeldingar – Meldingar sendt direkte frå spelet"

#: kchatdialog.cpp:126
#, kde-format
msgid "--- Game: "
msgstr "– Spel: "

#: kchatdialog.cpp:128
#, kde-format
msgid "This is a system message"
msgstr "Dette er ei systemmelding"

#: kchatdialog.cpp:132
#, kde-format
msgid "Maximum number of messages (-1 = unlimited):"
msgstr "Høgste tal på meldingar (-1 = uavgrensa):"

#. i18n: ectx: Menu (game)
#: kfourinlineui.rc:10
#, kde-format
msgid "&Game"
msgstr "&Spel"

#. i18n: ectx: Menu (settings)
#: kfourinlineui.rc:18
#, kde-format
msgid "&Settings"
msgstr "&Innstillingar"

#. i18n: ectx: ToolBar (mainToolBar)
#: kfourinlineui.rc:24
#, kde-format
msgid "Main Toolbar"
msgstr "Hovudverktøylinje"

#: kgameconnectdialog.cpp:63
#, kde-format
msgid "Create a network game"
msgstr "Lag eit nettverksspel"

#: kgameconnectdialog.cpp:66
#, kde-format
msgid "Join a network game"
msgstr "Vert med på eit nettverksspel"

#: kgameconnectdialog.cpp:72
#, kde-format
msgid "Game name:"
msgstr "Spelnamn:"

#: kgameconnectdialog.cpp:74
#, kde-format
msgid "Network games:"
msgstr "Nettverksspel:"

#: kgameconnectdialog.cpp:84
#, kde-format
msgid "Port to connect to:"
msgstr "Kopla til port:"

#: kgameconnectdialog.cpp:90
#, kde-format
msgid "Host to connect to:"
msgstr "Kopla til vert:"

#: kgameconnectdialog.cpp:95
#, kde-format
msgid "&Start Network"
msgstr "&Start nettverk"

#: kgamedebugdialog.cpp:118
#, kde-format
msgctxt "@title:window"
msgid "KGame Debug Dialog"
msgstr "KGame-avlusingsdialog"

#: kgamedebugdialog.cpp:140
#, kde-format
msgid "Debug &KGame"
msgstr "Avlus &KGame"

#: kgamedebugdialog.cpp:162 kgamedebugdialog.cpp:215
#, kde-format
msgid "Update"
msgstr "Oppdater"

#: kgamedebugdialog.cpp:167
#, kde-format
msgid "KGame Pointer"
msgstr "KGame-peikar"

#: kgamedebugdialog.cpp:168
#, kde-format
msgid "Game ID"
msgstr "Spel-ID"

#: kgamedebugdialog.cpp:169
#, kde-format
msgid "Game Cookie"
msgstr "Spelinfokapsel"

#: kgamedebugdialog.cpp:170
#, kde-format
msgid "Is Master"
msgstr "Er herre"

#: kgamedebugdialog.cpp:171
#, kde-format
msgid "Is Admin"
msgstr "Er administrator"

#: kgamedebugdialog.cpp:172
#, kde-format
msgid "Is Offering Connections"
msgstr "Tilbyr tilkoplingar"

#: kgamedebugdialog.cpp:173
#, kde-format
msgid "Game Status"
msgstr "Spelstatus"

#: kgamedebugdialog.cpp:174
#, kde-format
msgid "Game is Running"
msgstr "Spelet køyrer"

#: kgamedebugdialog.cpp:175
#, kde-format
msgid "Maximal Players"
msgstr "Høgste tal på spelarar"

#: kgamedebugdialog.cpp:176
#, kde-format
msgid "Minimal Players"
msgstr "Minste tal på spelarar"

#: kgamedebugdialog.cpp:177
#, kde-format
msgid "Players"
msgstr "Spelarar"

#: kgamedebugdialog.cpp:183
#, kde-format
msgid "Debug &Players"
msgstr "Avlus &spelarar"

#: kgamedebugdialog.cpp:193
#, kde-format
msgid "Available Players"
msgstr "Tilgjengelege spelarar"

#: kgamedebugdialog.cpp:219
#, kde-format
msgid "Player Pointer"
msgstr "Spelarpeikar"

#: kgamedebugdialog.cpp:220
#, kde-format
msgid "Player ID"
msgstr "Spelar-ID"

#: kgamedebugdialog.cpp:221
#, kde-format
msgid "Player Name"
msgstr "Spelarnamn"

#: kgamedebugdialog.cpp:222
#, kde-format
msgid "Player Group"
msgstr "Spelargruppe"

#: kgamedebugdialog.cpp:223
#, kde-format
msgid "Player User ID"
msgstr "Brukar-ID for spelar"

# skip-rule: personifisering
#: kgamedebugdialog.cpp:224
#, kde-format
msgid "My Turn"
msgstr "Min tur"

#: kgamedebugdialog.cpp:225
#, kde-format
msgid "Async Input"
msgstr "Asynk inn"

#: kgamedebugdialog.cpp:226
#, kde-format
msgid "KGame Address"
msgstr "KGame-adresse"

#: kgamedebugdialog.cpp:227
#, kde-format
msgid "Player is Virtual"
msgstr "Spelaren er virtuell"

#: kgamedebugdialog.cpp:228
#, kde-format
msgid "Player is Active"
msgstr "Spelaren er aktiv"

#: kgamedebugdialog.cpp:229
#, kde-format
msgid "RTTI"
msgstr "RTTI"

#: kgamedebugdialog.cpp:230
#, kde-format
msgid "Network Priority"
msgstr "Nettverksprioritet"

#: kgamedebugdialog.cpp:236
#, kde-format
msgid "Debug &Messages"
msgstr "Avlus &meldingar"

#: kgamedebugdialog.cpp:251
#, kde-format
msgid "&>>"
msgstr "&>>"

#: kgamedebugdialog.cpp:255
#, kde-format
msgid "&<<"
msgstr "&<<"

#: kgamedebugdialog.cpp:259
#, kde-format
msgid "Do not show IDs:"
msgstr "Ikkje vis ID-ar:"

#: kgamedebugdialog.cpp:328
#, kde-format
msgid "NULL pointer"
msgstr "NULL-peikar"

#: kgamedebugdialog.cpp:338 kgamedebugdialog.cpp:339 kgamedebugdialog.cpp:340
#: kgamedebugdialog.cpp:342 kgamedebugdialog.cpp:395 kgamedebugdialog.cpp:396
#: kgamedebugdialog.cpp:399 kgamedebugdialog.cpp:400
#, kde-format
msgid "True"
msgstr "Sann"

#: kgamedebugdialog.cpp:338 kgamedebugdialog.cpp:339 kgamedebugdialog.cpp:340
#: kgamedebugdialog.cpp:342 kgamedebugdialog.cpp:395 kgamedebugdialog.cpp:396
#: kgamedebugdialog.cpp:399 kgamedebugdialog.cpp:400
#, kde-format
msgid "False"
msgstr "Usann"

#: kgamedebugdialog.cpp:354 kgamedebugdialog.cpp:412
#, kde-format
msgid "Clean"
msgstr "Rydd opp"

#: kgamedebugdialog.cpp:357 kgamedebugdialog.cpp:415
#, kde-format
msgid "Dirty"
msgstr "Skitten"

#: kgamedebugdialog.cpp:360 kgamedebugdialog.cpp:418
#, kde-format
msgid "Local"
msgstr "Lokal"

#: kgamedebugdialog.cpp:364 kgamedebugdialog.cpp:422
#, kde-format
msgid "Undefined"
msgstr "Udefinert"

#: kgamedebugdialog.cpp:507
#, kde-format
msgid "Unknown"
msgstr "Ukjend"

#: kgamedialog.cpp:92
#, kde-format
msgid "&Network"
msgstr "&Nettverk"

#: kgamedialogconfig.cpp:124
#, kde-format
msgid "Disconnect"
msgstr "Kopla frå"

#: kgamedialogconfig.cpp:128 kwin4.cpp:680
#, kde-format
msgid "Network Configuration"
msgstr "Nettverksoppsett"

#: kgamedialogconfig.cpp:187
#, kde-format
msgid "Cannot connect to the network"
msgstr "Klarar ikkje kopla til nettverket"

#: kgamedialogconfig.cpp:193
#, kde-format
msgid "Network status: No Network"
msgstr "Nettverksstatus: Ikkje nettverk"

#: kgamedialogconfig.cpp:199
#, kde-format
msgid "Network status: You are MASTER"
msgstr "Nettverksstatus: Du er HERRE"

#: kgamedialogconfig.cpp:201
#, kde-format
msgid "Network status: You are connected"
msgstr "Nettverksstatus: Du er tilkopla"

#: kwin4.cpp:74
#, kde-format
msgid "Installation error: No theme list found."
msgstr "Installasjonsfeil: Fann ikkje temalista."

#: kwin4.cpp:101
#, kde-format
msgid "Installation error: No AI engine found. Continue without AI."
msgstr "Installasjonsfeil: Fann ingen AI-motor. Hald fram utan AI."

#: kwin4.cpp:116
#, kde-format
msgid "Installation error: Theme file error."
msgstr "Installasjonsfeil: Feil i temalista."

#: kwin4.cpp:264
#, kde-format
msgid "Ends a currently played game. No winner will be declared."
msgstr "Avslutt spelet som er i gang utan å kåra ein vinnar."

#: kwin4.cpp:269
#, kde-format
msgid "&Network Configuration..."
msgstr "&Nettverksoppsett …"

#: kwin4.cpp:273
#, kde-format
msgid "Network Chat..."
msgstr "Nettverksprat …"

#: kwin4.cpp:278
#, kde-format
msgid "&Show Statistics"
msgstr "&Vis statistikk"

#: kwin4.cpp:280
#, kde-format
msgid "Show statistics."
msgstr "Vis statistikk."

#: kwin4.cpp:292
#, kde-format
msgid "Theme"
msgstr "Tema"

#: kwin4.cpp:303
#, kde-format
msgid "Debug KGame"
msgstr "Avlus KGame"

#: kwin4.cpp:326
#, kde-format
msgid "Welcome to Four Wins"
msgstr "Velkommen til Fire på rad"

#: kwin4.cpp:486 kwin4.cpp:639
#, kde-format
msgid "Game running..."
msgstr "Spelet er i gang …"

#: kwin4.cpp:600
#, kde-format
msgid "No game  "
msgstr "Ingen spel  "

#: kwin4.cpp:602 kwin4.cpp:604
#, kde-format
msgid " %1 - %2 "
msgstr " %1 – %2 "

#: kwin4.cpp:606
#, kde-format
msgid "Nobody  "
msgstr "Ingen  "

#: kwin4.cpp:628
#, kde-format
msgid "The network game ended!\n"
msgstr "Nettverksspelet vart avslutta.\n"

#: kwin4.cpp:649
#, kde-format
msgid "The game is drawn. Please restart next round."
msgstr "Spelet er uavgjort. Start neste runde om att."

#: kwin4.cpp:656
#, kde-format
msgid "%1 won the game. Please restart next round."
msgstr "%1 vann spelet. Start neste runde om att."

#: kwin4.cpp:661
#, kde-format
msgid " Game ended. Please restart next round."
msgstr " Spel avbrote. Start neste runde om att."

#: kwin4.cpp:691
#, kde-format
msgid "Black should be played by remote player"
msgstr "Svart skal spelast av nettverksspelaren"

#: kwin4.cpp:692
#, kde-format
msgid "Red should be played by remote player"
msgstr "Raud skal spelast av nettverksspelaren"

#: kwin4.cpp:769 kwin4.cpp:770 kwin4.cpp:791 kwin4.cpp:792
#, kde-format
msgid "%1 Plays With"
msgstr "%1 speler med"

#: kwin4.cpp:793
#, kde-format
msgid "General"
msgstr "Generelt"

#. i18n: ectx: label, entry (port), group (Parameter)
#: kwin4.kcfg:10
#, kde-format
msgid "Network connection port"
msgstr "Port for nettverkstilkopling"

#. i18n: ectx: label, entry (gamename), group (Parameter)
#: kwin4.kcfg:14
#, kde-format
msgid "Game name"
msgstr "Spelnamn"

#. i18n: ectx: label, entry (host), group (Parameter)
#: kwin4.kcfg:17
#, kde-format
msgid "Network connection host"
msgstr "Vert for nettverkstilkopling"

#. i18n: ectx: property (whatsThis), widget (QSlider, kcfg_level)
#. i18n: ectx: label, entry (level), group (Parameter)
#: kwin4.kcfg:21 settings.ui:50
#, kde-format
msgid "Change the strength of the computer player."
msgstr "Endrar nivået til dataspelaren."

#. i18n: ectx: label, entry (Name1), group (Parameter)
#: kwin4.kcfg:28
#, kde-format
msgid "Player 1 name"
msgstr "Spelarnamn 1"

#: kwin4.kcfg:32
#, kde-format
msgctxt "default name of first player"
msgid "Player 1"
msgstr "Spelar 1"

#. i18n: ectx: label, entry (Name2), group (Parameter)
#: kwin4.kcfg:35
#, kde-format
msgid "Player 2 name"
msgstr "Spelarnamn 2"

#: kwin4.kcfg:39
#, kde-format
msgctxt "default name of second player"
msgid "Player 2"
msgstr "Spelar 2"

#: main.cpp:67
#, kde-format
msgid "KFourInLine"
msgstr "Fire på rad"

#: main.cpp:69
#, kde-format
msgid "KFourInLine: Two player board game"
msgstr "Brettspel for to spelarar"

#: main.cpp:71
#, kde-format
msgid "(c) 1995-2007, Martin Heni"
msgstr "© 1995–2007 Martin Heni"

#: main.cpp:72
#, kde-format
msgid "Martin Heni"
msgstr "Martin Heni"

#: main.cpp:72
#, kde-format
msgid "Game design and code"
msgstr "Spelutforming og programmering"

#: main.cpp:73
#, kde-format
msgid "Johann Ollivier Lapeyre"
msgstr "Johann Ollivier Lapeyre"

#: main.cpp:73 main.cpp:74
#, kde-format
msgid "Graphics"
msgstr "Bilete"

#: main.cpp:74
#, kde-format
msgid "Eugene Trounev"
msgstr "Eugene Trounev"

#: main.cpp:75
#, kde-format
msgid "Benjamin Meyer"
msgstr "Benjamin Meyer"

#: main.cpp:75
#, kde-format
msgid "Code Improvements"
msgstr "Kodeforbetringar"

#: main.cpp:80
#, kde-format
msgid "Enter debug level"
msgstr "Oppgje avlusingsnivå"

#: main.cpp:81
#, kde-format
msgid "Skip intro animation"
msgstr "Hopp over introanimasjon"

#: main.cpp:82
#, kde-format
msgid "Run game in demo (autoplay) mode"
msgstr "Køyr spelet i demomodus (automatisk speling)"

#: scoresprite.cpp:163
#, kde-format
msgctxt "computer level"
msgid "Level %1"
msgstr "Nivå %1"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox1)
#: settings.ui:38
#, kde-format
msgid "Computer Difficulty"
msgstr "Maskindugleik"

#. i18n: ectx: property (text), widget (QLabel, textLabel1)
#: settings.ui:83
#, kde-format
msgid "Easy"
msgstr "Lett"

#. i18n: ectx: property (text), widget (QLabel, textLabel2)
#: settings.ui:90
#, kde-format
msgid "Hard"
msgstr "Vanskeleg"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_learning)
#: settings.ui:102
#, kde-format
msgid "Use AI learning"
msgstr "Bruk AI-læring"

#. i18n: ectx: property (title), widget (QGroupBox, Input1)
#: settings.ui:112
#, kde-format
msgid "Red Plays With"
msgstr "Raud speler med"

#. i18n: ectx: property (text), widget (QRadioButton, kcfg_input1mouse)
#. i18n: ectx: property (text), widget (QRadioButton, kcfg_input0mouse)
#: settings.ui:132 settings.ui:181
#, kde-format
msgid "Mouse"
msgstr "Mus"

#. i18n: ectx: property (text), widget (QRadioButton, kcfg_input1key)
#. i18n: ectx: property (text), widget (QRadioButton, kcfg_input0key)
#: settings.ui:142 settings.ui:191
#, kde-format
msgid "Keyboard"
msgstr "Tastatur"

#. i18n: ectx: property (text), widget (QRadioButton, kcfg_input1ai)
#. i18n: ectx: property (text), widget (QRadioButton, kcfg_input0ai)
#: settings.ui:149 settings.ui:198
#, kde-format
msgid "Computer"
msgstr "Datamaskin"

#. i18n: ectx: property (title), widget (QGroupBox, Input0)
#: settings.ui:161
#, kde-format
msgid "Black Plays With"
msgstr "Svart spelar med"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox2)
#: settings.ui:216
#, kde-format
msgid "Player Names"
msgstr "Spelarnamn"

#. i18n: ectx: property (text), widget (QLabel, textLabel3)
#: settings.ui:242
#, kde-format
msgid "Player 1:"
msgstr "Spelar 1:"

#. i18n: ectx: property (text), widget (QLabel, textLabel4)
#: settings.ui:249
#, kde-format
msgid "Player 2:"
msgstr "Spelar 2:"

#. i18n: ectx: property (title), widget (QGroupBox, StartColour)
#: settings.ui:267
#, kde-format
msgid "Starting Player Color"
msgstr "Farge for spelaren som startar"

#. i18n: ectx: property (text), widget (QRadioButton, kcfg_startcolourred)
#: settings.ui:287
#, kde-format
msgid "Red"
msgstr "Raud"

#. i18n: ectx: property (text), widget (QRadioButton, kcfg_startcolouryellow)
#: settings.ui:297
#, kde-format
msgid "Black"
msgstr "Svart"

#. i18n: ectx: property (windowTitle), widget (QDialog, Statistics)
#: statistics.ui:13
#, kde-format
msgid "Statistics"
msgstr "Statistikk"

#. i18n: ectx: property (text), widget (QLabel, p1_name)
#: statistics.ui:57 statuswidget.ui:113
#, kde-format
msgid "Player 1"
msgstr "Spelar 1"

#. i18n: ectx: property (text), widget (QLabel, Name)
#: statistics.ui:64
#, kde-format
msgid "Name"
msgstr "Namn"

#. i18n: ectx: property (text), widget (QLabel, won)
#: statistics.ui:74
#, kde-format
msgid "Won"
msgstr "Vunne"

#. i18n: ectx: property (text), widget (QLabel, lost)
#: statistics.ui:87
#, kde-format
msgid "Lost"
msgstr "Tapt"

#. i18n: ectx: property (text), widget (QLabel, sum)
#: statistics.ui:103
#, kde-format
msgid "Sum"
msgstr "Sum"

#. i18n: ectx: property (text), widget (QLabel, aborted)
#: statistics.ui:113
#, kde-format
msgid "Aborted"
msgstr "Avbrote"

#. i18n: ectx: property (text), widget (QPushButton, pushButton1)
#: statistics.ui:131
#, kde-format
msgid "Clear All Statistics"
msgstr "Nullstill all statistikk"

#. i18n: ectx: property (text), widget (QPushButton, pushButton2)
#: statistics.ui:154
#, kde-format
msgid "&Close"
msgstr "Lu&kk"

#. i18n: ectx: property (text), widget (QLabel, p2_name)
#: statistics.ui:166 statuswidget.ui:220
#, kde-format
msgid "Player 2"
msgstr "Spelar 2"

#. i18n: ectx: property (text), widget (QLabel, drawn)
#: statistics.ui:176
#, kde-format
msgid "Drawn"
msgstr "Uavgjort"

#. i18n: ectx: property (text), widget (QLabel, wins)
#: statuswidget.ui:63
#, kde-format
msgid "W"
msgstr "V"

#. i18n: ectx: property (text), widget (QLabel, draws)
#: statuswidget.ui:73
#, kde-format
msgid "D"
msgstr "U"

#. i18n: ectx: property (text), widget (QLabel, loses)
#: statuswidget.ui:83
#, kde-format
msgid "L"
msgstr "T"

#. i18n: ectx: property (text), widget (QLabel, num)
#: statuswidget.ui:93
#, kde-format
msgid "No"
msgstr "Nei"

#. i18n: ectx: property (text), widget (QLabel, bk)
#: statuswidget.ui:103
#, kde-format
msgid "Bk"
msgstr "Svt"
